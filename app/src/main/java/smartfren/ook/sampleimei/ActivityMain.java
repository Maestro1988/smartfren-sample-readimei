package smartfren.ook.sampleimei;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

/**
 * Created by nugrohopriambodo on 8/26/15.
 */
public class ActivityMain extends Activity {

    private TextView mainMEID;
    private TextView mainIMEI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mainactivity);
        mainMEID = (TextView)findViewById(R.id.main_meid);
        mainIMEI = (TextView)findViewById(R.id.main_imei);
        DualSIMManager dualSIMManager = new DualSIMManager(this);
        mainMEID.setText("MEID: " + dualSIMManager.getIMEI(0));
        mainIMEI.setText("IMEI: " + dualSIMManager.getIMEI(1));
    }
}
